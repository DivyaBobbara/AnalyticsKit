//
//  Provider.swift
//  AnalyticsKit
//
//  Created by Naga Divya Bobbara on 03/02/24.
//

import Foundation


enum Provider: String {
    case mixpanel = "MIXPANEL"
    case firebaseAnalytics = "FIREBASE"
}
