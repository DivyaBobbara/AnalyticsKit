//
//  AnalyticsTrackable.swift
//  AnalyticsKit
//
//  Created by Naga Divya Bobbara on 01/02/24.
//

import Foundation

protocol AnalyticsEvent {
    var eventName: String { get }
    var eventProperties: [String:Any] { get }
    var providers: [String] { get }
}
