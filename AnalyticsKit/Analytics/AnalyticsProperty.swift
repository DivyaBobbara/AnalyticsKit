//
//  AnalyticsProperties.swift
//  AnalyticsKit
//
//  Created by Naga Divya Bobbara on 01/02/24.
//

import Foundation

protocol AnalyticsProperty {
    var providers: [String] { get }
    var properties: [String: Any] { get }
}
