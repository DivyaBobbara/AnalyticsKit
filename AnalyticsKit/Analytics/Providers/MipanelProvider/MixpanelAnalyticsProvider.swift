//
//  MixpanelAnalyticsProvider.swift
//  AnalyticsKit
//
//  Created by Naga Divya Bobbara on 01/02/24.
//

import Foundation
import Mixpanel


class MixpanelAnalyticsProvider: AnalyticsProvider {
    
    private var mixpanel: MixpanelInstance? = nil
    
    init(token: String, instanceName: String) {
        self.mixpanel = Mixpanel.initialize(token: token, trackAutomaticEvents: true, instanceName: instanceName)
    }
    
    func setProperties(properties: AnalyticsProperty, userId: String) {
        mixpanel?.registerSuperProperties(properties.properties as? Properties ?? Properties())
    }
    
    func setUserProfile(properties: AnalyticsProperty, userId: String) {
        mixpanel?.identify(distinctId: userId)
        mixpanel?.people.set(properties: properties.properties as? Properties ?? Properties())
    }
    
    func trackEvent(event: AnalyticsEvent) {
        mixpanel?.track(event: event.eventName, properties: event.eventProperties as? Properties)
    }
    
    func reset() {
        mixpanel?.clearSuperProperties()
        mixpanel?.reset()
    }
    
}
