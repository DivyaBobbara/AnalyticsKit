//
//  Analytics.swift
//  AnalyticsKit
//
//  Created by Naga Divya Bobbara on 01/02/24.
//

import Foundation

protocol AnalyticsProvider {
    func setProperties(properties: AnalyticsProperty, userId: String)
    func setUserProfile(properties: AnalyticsProperty, userId: String)
    func trackEvent(event: AnalyticsEvent)
    func reset()
}
