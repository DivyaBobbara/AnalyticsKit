//
//  Analytics.swift
//  AnalyticsKit
//
//  Created by Naga Divya Bobbara on 03/02/24.
//

import Foundation

protocol Analytics {
    func setProperties(properties: AnalyticsProperty, userId: String)
    func setUserProfile(properties: AnalyticsProperty, userId: String)
    func sendEvent(event: AnalyticsEvent)
    func reset()
}

