//
//  AnalyticsImplementation.swift
//  AnalyticsKit
//
//  Created by Naga Divya Bobbara on 03/02/24.
//

import Foundation
import Mixpanel

class AnalyticsImplementation: Analytics {
    
    let mixpanelProvider = MixpanelAnalyticsProvider(token: "", instanceName: "")
    
    func setProperties(properties: AnalyticsProperty, userId: String) {
        if properties.providers.contains(Provider.mixpanel.rawValue) {
            mixpanelProvider.setProperties(properties: properties, userId: userId)
        }
    }
    
    func setUserProfile(properties: AnalyticsProperty, userId: String) {
        if properties.providers.contains(Provider.mixpanel.rawValue) {
            mixpanelProvider.setUserProfile(properties: properties, userId: userId)
        }
    }
    
    func sendEvent(event: AnalyticsEvent) {
        if event.providers.contains(Provider.mixpanel.rawValue) {
            mixpanelProvider.trackEvent(event: event)
        }
    }
    
    func reset() {
        mixpanelProvider.reset()
    }
}
